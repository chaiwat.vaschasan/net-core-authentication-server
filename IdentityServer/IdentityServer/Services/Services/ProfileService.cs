﻿using IdentityModel;
using IdentityServer.Entities;
using IdentityServer.Services.Interface;
using IdentityServer4.Models;
using IdentityServer4.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer.Services.Services
{
    public class ProfileService : IProfileService
    {
        private readonly IAccountService accountService;

        public ProfileService(IAccountService accountService) 
        {
            this.accountService = accountService;
        }
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {

            try
            {
                if (!string.IsNullOrEmpty(context.Subject.Identity.Name))
                {
                    Account user = await accountService.GetByUsernameAsync(context.Subject.Identity.Name);
                    
                    Claim[] claims = new Claim[]
                    {
                        new Claim(ClaimTypes.Name,user.Username),
                        new Claim(ClaimTypes.Role,user.Role.Name),
                        new Claim(ClaimTypes.Email,user.Email),
                        new Claim(ClaimTypes.Sid,user.AccountId.ToString()),

                    };
                    context.IssuedClaims = claims.ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public async Task IsActiveAsync(IsActiveContext context)
        {
            try
            {
                if (!string.IsNullOrEmpty(context.Subject.Identity.Name))
                {
                    Account user = await accountService.GetByUsernameAsync(context.Subject.Identity.Name);
                    if (user != null)
                    {
                        context.IsActive = true;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
