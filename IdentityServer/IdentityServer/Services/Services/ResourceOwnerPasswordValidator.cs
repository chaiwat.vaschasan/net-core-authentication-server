﻿using IdentityModel;
using IdentityServer.Entities;
using IdentityServer.Services.Interface;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer.Services.Services
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly IAccountService accountService;

        public ResourceOwnerPasswordValidator(IAccountService accountService ) 
        {
            this.accountService = accountService;
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            try
            {
                Account user = await accountService.GetAccountAsync(context.UserName, context.Password);


                if (user != null)
                {
                    Claim[] claims = new Claim[]
                    {
                        new Claim(ClaimTypes.Name,user.Username),
                        new Claim(ClaimTypes.Role,user.Role.Name),
                        new Claim("roles",user.Role.Name),
                        new Claim(ClaimTypes.Email,user.Email),
                        new Claim(ClaimTypes.Sid,user.AccountId.ToString()),
                    };
                    context.Result = new GrantValidationResult(
                        subject: user.AccountId.ToString(),
                        authenticationMethod: "custom",
                        claims: claims);
                }
                else
                {
                    context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "User does not exist.");
                }
            }
            catch (Exception ex)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Invalid username or password");
                throw ex;
            }

        }

    }
}
