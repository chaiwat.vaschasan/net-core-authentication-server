﻿using IdentityServer.Entities;
using IdentityServer.Services.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Services.Services
{
    public class AccountService : IAccountService 
    {
        private readonly EntityContext entity;

        public AccountService(EntityContext entity) 
        {
            this.entity = entity;
        }

        public Account GetAccount(string username, string password)
        {
            return  entity.Accounts
                        .Where(x => x.Username.Equals(username) && x.Password.Equals(password))
                        .FirstOrDefault();
        }

        public async Task<Account> GetAccountAsync(string username, string password)
        {
            return await entity.Accounts.Where(x => x.Username.Equals(username) && x.Password.Equals(password))
                .Include(x => x.Role)
                .FirstOrDefaultAsync();
        }

        public async Task<Account> GetByUsernameAsync(string username)
        {
            return await entity.Accounts.Where(x => x.Username.Equals(username))
                .Include(x => x.Role)
                .FirstOrDefaultAsync();
        }

        public  void Register(Account account)
        {
            Account duplicate = entity.Accounts
                .Where(x => x.Username.Equals(account.Username) || x.Email.Equals(account.Email))
                .FirstOrDefault();

            if (duplicate != null) throw new Exception("This account is already in the system.");
            account.AccountId = Guid.NewGuid();

            entity.Accounts.Add(account);
            entity.SaveChanges();
        }

        public void Update(Account account)
        {
            Account updateItem = entity.Accounts.Find(account.AccountId);

            if (updateItem == null) throw new Exception("Not found account");

            Account duplicate = entity.Accounts
                .Where(x => x.Email.Equals(account.Email) && ! x.AccountId.Equals(updateItem.AccountId))
                .FirstOrDefault();

            if(duplicate != null ) throw new Exception("This email is already in the system.");

            updateItem.Password = account.Password;
            updateItem.Tel = account.Tel;
            updateItem.Email = account.Email;
            updateItem.RoleId = account.RoleId;
            entity.SaveChanges();

        }
    }
}
