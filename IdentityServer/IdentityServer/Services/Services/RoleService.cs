﻿using IdentityServer.Entities;
using IdentityServer.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Services.Services
{
    public class RoleService : IRoleService
    {
        private readonly EntityContext entities;

        public RoleService(EntityContext entities) 
        {
            this.entities = entities;
        }

        public void Add(Role role)
        {
            Role duplicate = entities.Roles.Where(x => x.Name.Equals(role.Name)).FirstOrDefault();
            if (duplicate != null) throw new Exception($"This role {role.Name} is already in the system.");

            role.CreationDate = DateTime.Now;
            role.UpdateDate = DateTime.Now;
            entities.Roles.Add(role);
            entities.SaveChanges();

        }

        public List<Role> GetAll()
        {

            return entities.Roles.ToList();
        }

        public Role GetById(int id)
        {
            return entities.Roles.Find(id);
        }

        public void Update(Role role)
        {

            Role updateItem = GetById(role.RoleId);
            if (updateItem == null) throw new Exception($"Not found role id {role.RoleId} ");
            updateItem.Name = role.Name;
            updateItem.Description = role.Description;
            updateItem.UpdateDate = DateTime.Now;
        }
    }
}
