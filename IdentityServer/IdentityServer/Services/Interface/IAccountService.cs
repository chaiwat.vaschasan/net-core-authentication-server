﻿using IdentityServer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Services.Interface
{
    public interface IAccountService
    {
        void Register(Account account);
        Account GetAccount(string username , string password);
        void Update(Account account);
        Task<Account> GetByUsernameAsync(string username);
        Task<Account> GetAccountAsync(string username, string password);
    }
}
