﻿using IdentityServer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Services.Interface
{
    public interface IRoleService
    {
        List<Role> GetAll();
        Role GetById(int id);
        void Update(Role role);
        void Add(Role role);

    }
}
