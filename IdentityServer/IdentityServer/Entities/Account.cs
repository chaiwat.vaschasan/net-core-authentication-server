﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Entities
{
    public class Account 
    {
        [Key]
        public Guid AccountId { get; set; }
        [Required]
        [MaxLength(50)]
        public string Username { get; set; }
        [Required]
        [MaxLength(100)]
        public string Password { get; set; }
        [Required]
        [MaxLength(150)]
        public string Email { get; set; }
        [MaxLength(10)]
        public string Tel { get; set; }
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }

    }
}
