﻿using IdentityServer.Entities;
using IdentityServer.Services.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Controllers
{
    [Route("api/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService accountService;

        public AccountController(IAccountService accountService) 
        {
            this.accountService = accountService;
        }

        [HttpPost]
        [Route("register")]
        public IActionResult Register([FromBody] Account account) 
        {
            try
            {
                accountService.Register(account);
                return NoContent();
            }
            catch (Exception e) 
            {
                return BadRequest(e.Message);
            }
        }
    }
}
