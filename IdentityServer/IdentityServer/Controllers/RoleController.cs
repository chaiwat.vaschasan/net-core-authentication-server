﻿using IdentityServer.Entities;
using IdentityServer.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Controllers
{
    [Route("api/role")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService roleService;

        public RoleController(IRoleService roleService) 
        {
            this.roleService = roleService;
        }

        [HttpPost]
        [Route("add")]
        [Authorize(Roles = "ADMIN" )]
        public IActionResult AddRole([FromBody] Role role) 
        {
            try
            {
                roleService.Add(role);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut]
        [Route("update")]
        [Authorize(Roles = "ADMIN")]
        public IActionResult UpdateRole([FromBody]Role role) 
        {
            try
            {
                roleService.Update(role);
                return NoContent();
            }
            catch (Exception e) 
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("getAll")]
        [Authorize]
        public IActionResult GetAll() 
        {
            return Ok(roleService.GetAll());
        }

        [HttpGet]
        [Route("get")]
        [Authorize]
        public IActionResult Get(int id) 
        {
            return Ok(roleService.GetById(id));
        }
    }
}
