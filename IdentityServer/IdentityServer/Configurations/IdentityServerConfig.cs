﻿using IdentityServer.Services.Services;
using IdentityServer4;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Configurations
{
    public static class IdentityServerConfig
    {
        public static void IdentityConfig(this IServiceCollection services, IConfiguration configuration) 
        {
            string client = configuration.GetConnectionString("ClientId");
            string secrets = configuration.GetConnectionString("ClientSecrets");

            services.AddIdentityServer()
                    .AddInMemoryApiResources(GetApis())
                    .AddInMemoryClients(GetClients(client, secrets))
                    .AddInMemoryApiScopes(GetApiScopes())
                    .AddDeveloperSigningCredential();

            services.AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator>();
            services.AddTransient<IProfileService, ProfileService>();

            services.AddAuthentication("Bearer").AddJwtBearer("Bearer",
             options =>
             {
                 options.Authority = "http://localhost:5000";
                 options.Audience = "api1";
                 options.RequireHttpsMetadata = false;

                 options.TokenValidationParameters = new TokenValidationParameters()
                 {
                     ValidateAudience = false
                 };
             });
        }

        public static IEnumerable<Client> GetClients(string clientId, string clientSecrets)
        {
            return new List<Client>
            {
                // resource owner password grant client
                new Client
                {
                    ClientId = clientId,
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    ClientSecrets = { new Secret(clientSecrets.Sha256())},
                    AllowedScopes = new List<string>
                    {
                        "api1",
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                    },
                    AllowOfflineAccess = true,
                    AbsoluteRefreshTokenLifetime = 604800
                }
            };
        }

        public static IEnumerable<ApiResource> GetApis()
        {
            return new List<ApiResource>
            {
                new ApiResource("api1", "My API")
            };
        }

        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>
             {
                 new ApiScope(name: "api1",   displayName: "My API"),
             };
        }


    }
}
