﻿using IdentityServer.Services.Interface;
using IdentityServer.Services.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Configurations
{
    public static class ServiceDependencyConfig
    {
        public static void RegisterService(this IServiceCollection services) 
        {
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IAccountService, AccountService>();

        }
    }
}
